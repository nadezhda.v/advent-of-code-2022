import { readFile } from "fs";

export function day02() {
  readFile("src/day02/day02.txt", "utf8", (err, data) => {
    if (err) return err;
    const games = data.split("\n");
    console.log("массив", games);
    let score = 0;

    for (const game of games) {
      score += SecondGame(game[0], game[2]);
      console.log("ход игрока", game[0], "мой ход", game[2]);
    }
    console.log({ score });
  });
}

const playGame = (playerChoice: string, myChoice: string): number => {
  const scoreFromChoice = choiceScore(myChoice);
  const scoreFromGame = gameScore(playerChoice, myChoice);
  return scoreFromGame + scoreFromChoice;
};

function choiceScore(myChoice) {
  let counter = 0;

  if (myChoice === "X") counter += 1;
  if (myChoice === "Y") counter += 2;
  if (myChoice === "Z") counter += 3;

  return counter;
}

function gameScore(playerChoice: string, myChoice: string): number {
  let counter = 0;

  if (playerChoice === "A") {
    if (myChoice === "X") counter += 3;
    if (myChoice === "Y") counter += 6;
    if (myChoice === "Z") counter += 0;
  }

  if (playerChoice === "B") {
    if (myChoice === "X") counter += 0;
    if (myChoice === "Y") counter += 3;
    if (myChoice === "Z") counter += 6;
  }

  if (playerChoice === "C") {
    if (myChoice === "X") counter += 6;
    if (myChoice === "Y") counter += 0;
    if (myChoice === "Z") counter += 3;
  }

  return counter;
}

///часть два

const SecondGame = (userChoice: string, gameResult: string) => {
  let counter = 0;

  if (gameResult === "X") {
    if (userChoice === "A") counter += 3;
    if (userChoice === "B") counter += 1;
    if (userChoice === "C") counter += 2;
  }

  if (gameResult === "Y") {
    counter += 3;
    if (userChoice === "A") counter += 1;
    if (userChoice === "B") counter += 2;
    if (userChoice === "C") counter += 3;
  }

  if (gameResult === "Z") {
    counter += 6;
    if (userChoice === "A") counter += 2;
    if (userChoice === "B") counter += 3;
    if (userChoice === "C") counter += 1;
  }

  return counter;
};
